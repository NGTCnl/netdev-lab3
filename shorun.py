from netmiko import Netmiko
from datetime import date
today = str(date.today())

RTR1 = {
    "host": "10.1.1.10",
    "username": "onezero",
    "use_keys": True,
    "key_file": "/home/onezero1/.ssh/cisco",
    "device_type": "cisco_ios",
}

RTR2 = {
    "host": "10.1.1.11",
    "username": "onezero",
    "use_keys": True,
    "key_file": "/home/onezero1/.ssh/cisco",
    "device_type": "cisco_ios",
}

cmd = "show run"

for device in (RTR1, RTR2):
    net_conn = Netmiko(**device)
    print(net_conn.find_prompt())

    output = net_conn.send_command(cmd)
    net_conn.disconnect()

    print(output)
    host = device["host"]
    filename = host+"-"+today+".config"

    with open(filename, "w") as output_file:
        output_file.write(output)


