import napalm
from pprint import pprint
import netboxget

all_devices = netboxget.all_devices
driver = napalm.get_network_driver("ios")

print("{:14}{:10}{:14}{:95}{:12}".format("Hostname", "Model", "Serial", "Ver", "uptime"))
print(145 * "-")

for host in all_devices:
    device = driver(
        hostname=host,
        username="onezero",
        password=None,
        optional_args={"use_keys": True, "key_file": "/home/onezero3/.ssh/cisco"},
    )

    # open session to device
    device.open()
    # execute commands
    facts = device.get_facts()
    # close the session
    device.close()

    #pprint(facts)

    dev_name = facts['hostname']
    dev_model = facts['model']
    dev_serial = facts['serial_number']
    dev_ver = facts['os_version']
    dev_uptime = facts['uptime']
    print("{:14}{:10}{:14}{:95}{:12}".format(dev_name, dev_model, dev_serial, dev_ver, dev_uptime))


