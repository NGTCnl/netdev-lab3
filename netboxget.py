import requests
import json
from pprint import pprint

headers = {
    'accept': 'application/json',
    'Authorization': 'Token e67b72eb4f37b460d8001906049e4c85b85648ca',
}

netbox_url = 'https://netbox.interestingtraffic.nl/api/dcim/devices/?role=router'
response = requests.get(netbox_url, headers=headers)

results = json.loads(response.text)

#pprint(results['results'])

print("{:15}{:15}{:30}".format("Device", "Type", "IP address"))
all_devices = []
for i in results['results']:
    dev_name = i['name']
    dev_type = i['device_type']['model']
    dev_ip = i['primary_ip']['address']
    print("{:15}{:15}{:30}".format(dev_name, dev_type, dev_ip))
    real_ip = dev_ip.split("/")[0]
    all_devices.append(real_ip)

print("Devices to be imported into NAPALM:")
print(all_devices)
